<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::namespace('FrontEnd')->group(function () {
    // Controllers Within The "App\Http\Controllers\FrontEnd" Namespace
    Route::get('/', 'FrontEndController@index')->name('home');
});

Auth::routes();

Route::middleware('auth')->namespace('Dashboard')->prefix('dashboard')->group(function () {
    // Controllers Within The "App\Http\Controllers\Dashboard" Namespace
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::namespace('Product')->group(function (){
        Route::resource('/product' , 'ProductController');
        Route::post('/buy_product/{product}','ProductController@buy_product')->name('buy_product');
    });
    Route::namespace('User')->group(function (){
        Route::resource('/user' , 'UserController');
    });
});
