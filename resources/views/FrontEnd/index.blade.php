@extends('layouts.Frontend.FrontEnd')

@section('content')
    <section class="jumbotron text-center">
        <div class="container">
            <h1>Welcome To My Site</h1>
            <p class="lead text-muted">Overrate to your thoughts, they will be your words, these words will be your
                behavior, overrate to your behavior they will be your personality.</p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                @foreach($product as $item)
                    <div class="col-md-4">
                        <div class="card mb-4 shadow-sm">
                            <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                                 xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"
                                 focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>
                                    Placeholder</title>
                                <rect width="100%" height="100%" fill="#55595c"/>
                                <text x="50%" y="50%" fill="#eceeef" dy=".3em">{{ $item->name }}</text>
                            </svg>
                            <div class="card-body">
                                <p class="card-text">{{ $item->description }}.</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">

                                        <button type="button" class="btn btn-sm btn-outline-secondary buy"
                                                data-id="{{ $item->id }}">Buy
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $('.buy').on('click', function () {
            $.ajax({
                type: 'POST',
                url: 'dashboard/buy_product/' + $(this).data('id'),
                data:{_token:'{{ csrf_token() }}'},
                success: function (data) {
                    alert('You bought this product successfully!');
                },
                error: function (data) {
                    if(data.status == 401){
                        alert('Please login first');
                    }
                }
            });
        })
    </script>
@endpush
