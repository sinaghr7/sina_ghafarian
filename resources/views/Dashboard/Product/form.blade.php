<div class="form-group">
    <label for="Product_Name">Product_name</label>
    <input type="text" class="form-control" id="Product_Name" placeholder="name" name="name" value="{{ $product->name }}">
</div>
<div class="form-group">
    <label for="description">description</label>
    <textarea class="form-control" id="description" rows="3" name="description">{{ $product->description }}</textarea>
</div>
<button class="form-control btn btn-primary" type="submit">submit</button>

