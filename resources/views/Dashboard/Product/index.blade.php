@extends('layouts.Dashboard.Dashboard')

@section('content')
    <h2>Products List</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Description</th>
                <th>Controller</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->description }}</td>
                    <td>
                        <div class="row">
                            <div class="col-md-1">
                                <a class="btn btn-primary" href=" {{ route('product.edit',$item->id) }} ">Edit</a>
                            </div>
                            <div class="col-md-2">
                                <a class="btn btn-danger delete-form" role="button" data-id="{{ $item->id }}">Delete</a>
                            </div>
                            <div class="col-md-8">
                                <a class="btn btn-warning" data-toggle="collapse" href="#collapseExample{{$item->id}}"
                                   role="button"
                                   aria-expanded="false" aria-controls="collapseExample">
                                    Shoppers
                                </a>
                                <div class="collapse" id="collapseExample{{$item->id}}">
                                    <div>
                                        @foreach($item->shoppers as $item)
                                            <span class="badge badge-secondary shoppers-badge">{{ $item->name }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a class="btn btn-success" href="{{ route('product.create') }}">Create a new</a>
    </div>
@endsection
@push('scripts')

    <script>
        $('.delete-form').on('click', function () {
            $.ajax({
                type: 'POST',
                url: '/dashboard/product/' + $(this).data('id'),
                data: {_method: 'delete',_token: '{{csrf_token()}}'},
                success: function (data) {
                    alert('Product deleted successfully!');
                    location.reload();
                }
            });
        })
    </script>
@endpush
