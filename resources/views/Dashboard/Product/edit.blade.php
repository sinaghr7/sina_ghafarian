@extends('layouts.Dashboard.Dashboard')

@section('content')

    <form method="POST" action=" {{ route('product.update', $product->id) }} ">
        @csrf
        @include('Dashboard.Product.form')
        @method('PATCH')
    </form>

@endsection
