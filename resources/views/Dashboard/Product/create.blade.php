@extends('layouts.Dashboard.Dashboard')

@section('content')

    <form method="POST" action=" {{ route('product.store') }} ">
        @csrf
        @include('Dashboard.Product.form')
    </form>

@endsection
