@extends('layouts.Dashboard.Dashboard')

@section('content')

    <form method="POST" action=" {{ route('user.update',$user->id) }} ">
        @csrf
        @include('Dashboard.User.form')
        @method('PATCH')
    </form>

@endsection
