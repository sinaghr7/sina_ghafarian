@extends('layouts.Dashboard.Dashboard')

@section('content')
    <h2>Customers List</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Controllers</th>
            </tr>
            </thead>
            <tbody>
            @foreach($user as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>
                        <a class="btn btn-primary" href="{{ route('user.edit',$item->id) }}">Edit</a>
                        <a class="btn btn-danger delete_user" role="button" data-id="{{ $item->id }}">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@push('scripts')

    <script>
        $('.delete_user').on('click', function () {
            $.ajax({
                type: 'POST',
                url: '/dashboard/user/' + $(this).data('id'),
                data: {_method: 'delete',_token: '{{csrf_token()}}'},
                success: function (data) {
                    alert('User deleted successfully!');
                    location.reload();
                }
            });
        })
    </script>
@endpush
