<?php

namespace App\Http\Controllers\Dashboard\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    public function index()
    {
        $user = User::all();
        return view('Dashboard.User.index',compact('user'));
    }
    public function edit(User $user)
    {
        return view('/Dashboard.User.edit',compact('user'));
    }

    public function update(User $user)
    {
        $user->name = request('name');
        $user->email = request('email');
        $user->save();
        return redirect('/dashboard/user');
    }
    public function destroy(User $user)
    {
        $user->delete();
        return redirect('/dashboard/user');
    }
}
