<?php

namespace App\Http\Controllers\Dashboard\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    public function index()
    {
        $product = Product::all();

        return view('Dashboard.Product.index', compact('product'));
    }

    public function create()
    {
        $product = new Product();
        return view('Dashboard.Product.create', compact('product'));
    }

    public function store()
    {
        $product = new Product();
        $product->name = request('name');
        $product->description = request('description');
        $product->save();
        return redirect('/dashboard/product');
    }

    public function edit(Product $product)
    {
        return view('Dashboard.Product.edit', compact('product'));
    }

    public function update(Product $product)
    {
        $product->name = request('name');
        $product->description = request('description');
        $product->save();
        return redirect('/dashboard/product');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('/dashboard/product');
    }

    public function buy_product(Product $product)
    {
        $product->shoppers()->attach(Auth::user()->id);
        return Response::HTTP_OK;
    }
}
