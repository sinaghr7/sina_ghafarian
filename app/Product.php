<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $guarded = [];

    function shoppers()
    {
        return $this->belongsToMany('App\User', 'product_user', 'product_id', 'user_id');
    }
}
